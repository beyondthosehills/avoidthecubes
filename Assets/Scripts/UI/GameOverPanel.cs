﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31.MessageKit;
using AvoidTheCube;

namespace AvoidTheCubes.UI
{

    public class GameOverPanel : MonoBehaviour 
    {

    	// Use this for initialization
    	void Awake () 
        {
            MessageKit.addObserver (MessageType.GameOver, ShowPanel);
            HidePanel ();
    	}

        void OnDestroy ()
        {
            MessageKit.removeObserver (MessageType.GameOver, ShowPanel);
        }

        void HidePanel ()
        {
            this.gameObject.SetActive (false);
        }

        void ShowPanel ()
        {
            this.gameObject.SetActive (true);
        }
    	
    	
    }
}
