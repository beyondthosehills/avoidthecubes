﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Prime31.MessageKit;
using AvoidTheCube;

namespace AvoidTheCubes.UI
{

    public class LifeProgressBar : MonoBehaviour 
    {

        [SerializeField]
        Image progressBarImage;

    	// Use this for initialization
    	void Awake () 
        {
            progressBarImage.fillAmount = 1f;

            MessageKit<int,int>.addObserver (MessageType.UpdateLife, UpdateLifeProgress);
    	}

        void OnDestroy ()
        {
            MessageKit<int,int>.removeObserver (MessageType.UpdateLife, UpdateLifeProgress);
        }
    	
        void UpdateLifeProgress(int currentLife, int totalLife)
        {
            progressBarImage.fillAmount = (float)currentLife / (float)totalLife;
        }
    }
}
