﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMessageManager : MonoBehaviour {

    [SerializeField]
    GameObject uiObject;
    [SerializeField]
    Text messageText;
    [SerializeField]
    Button continueButton;

    private static UIMessageManager instance;

	// Use this for initialization
	void Awake () 
    {
        if (instance == null)
            instance = this;

        continueButton.onClick.AddListener (hide);

        hide ();
	}

    public static void Show (string message)
    {
        if (instance == null)
            return;

        instance.show (message);
    }

    void show (string message)
    {
        messageText.text = message;
        uiObject.SetActive (true);
    }
	
    void hide ()
    {
        uiObject.SetActive (false);
    }
}
