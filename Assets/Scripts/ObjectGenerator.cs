﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AvoidTheCube
{
    public class ObjectGenerator : MonoBehaviour 
    {
        [SerializeField]
        GameObject[] objects;
        [SerializeField]
        float nextSpawnDuration = 5f;
        [SerializeField]
        float spawnRange = 5f;

        float timer = 0f;

    	// Use this for initialization
    	void Start () 
        {
    		
    	}
    	
    	// Update is called once per frame
    	void Update () 
        {
            timer += Time.deltaTime;

            if (timer > nextSpawnDuration)
            {
                RandomSpawn ();
                timer = 0f;
            }
    	}

        void RandomSpawn ()
        {
            Debug.Log ("Spawn");

            if (objects == null || objects.Length == 0)
                return;

            int randomIndex = Random.Range (0, objects.Length);

            GameObject objectToSpawn = objects[randomIndex];
            Vector3 spawnPosition = this.transform.position + (Vector3.right * Random.Range (spawnRange / 2f * -1f, spawnRange / 2f));

            Instantiate<GameObject> (objectToSpawn, spawnPosition, Quaternion.identity);
        }

        void OnDrawGizmos ()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawCube (this.transform.position, new Vector3(spawnRange,1f,1f));
        }
    }
}
