﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31.MessageKit;

namespace AvoidTheCube
{
    public class Player : MonoBehaviour 
    {
        [SerializeField]
        int initLife = 3;
        [SerializeField]
        float speed = 1f;

        int life;

    	// Use this for initialization
    	void Start () 
        {
            life = initLife;
    	}

        void OnTriggerEnter (Collider collider)
        {
            Debug.Log ("Object collider");

            GameObject otherObject = collider.gameObject;

            if (otherObject.GetComponent<Enemy> () != null)
            {
                ReduceLife ();
                DestroyObject (otherObject);
            }
        }

        void ReduceLife ()
        {
            life--;
            if (life == 0)
            {
                MessageKit.post (MessageType.GameOver);
            }

            MessageKit<int,int>.post (MessageType.UpdateLife, life, initLife);
        }
    	
    	// Update is called once per frame
    	void Update () 
        {
            Vector3 direction = Vector3.right * Input.GetAxis ("Horizontal");
            transform.position = transform.position + (direction * (speed * Time.deltaTime));
    	}
    }
}