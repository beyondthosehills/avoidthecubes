using UnityEngine;
using System.Collections;

[AddComponentMenu("TOOLS/Gizmo Collider")]
public class GizmoCollider : MonoBehaviour {

	// public variables
	public Color color;

	// private variables
	private Collider 	thisCollider;
	private float		size;

	void OnDrawGizmos () {
		
		MyGizmo();
	}
	
	void OnDrawGizmosSelected () {
		
		MyGizmo();
	}
	
	private void MyGizmo() 
	{
		
		Gizmos.color = color;
		Gizmos.matrix = this.transform.localToWorldMatrix;

		thisCollider = this.GetComponent<Collider>();

		if(thisCollider != null) 
		{

			if(thisCollider.GetType() == typeof(BoxCollider)) 
			{
				Gizmos.DrawCube(((BoxCollider)thisCollider).center, ((BoxCollider)thisCollider).size );
			}

			if(thisCollider.GetType() == typeof(SphereCollider)) 
			{
				Gizmos.DrawSphere(((SphereCollider)thisCollider).center, ((SphereCollider)thisCollider).radius);
			}
		}

		Gizmos.matrix = Matrix4x4.identity;
		Gizmos.color = Color.blue;
		//Gizmos.DrawRay(this.transform.position, this.transform.forward * this.transform.localScale.x);
	}
}
