﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleBehaviourTools
{
    public class ActivationBehaviour : GenericBehaviour 
    {

        [SerializeField]
        GameObject[] activateList;
        [SerializeField]
        GameObject[] deactivateList;

        public override void Behave ()
        {
            if (activateList != null)
            {
                foreach (GameObject _object in activateList)
                {
                    if (_object != null)
                        _object.SetActive (true);
                }
            }

            if (deactivateList != null)
            {
                foreach (GameObject _object in deactivateList)
                {
                    if (_object != null)
                        _object.SetActive (false);
                }
            }
        } 
    }
}
