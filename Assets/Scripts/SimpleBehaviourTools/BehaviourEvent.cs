﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleBehaviourTools
{

    public class BehaviourEvent : MonoBehaviour 
    {
        public enum EventType
        {
            OnMouseUpAsButton = 0,

            OnTriggerEnter = 10,
            OnTriggerExit = 11,
        }

        public enum EventMode
        {
            Once = 0,
            EveryTime = 1,
        }

        [SerializeField]
        EventType type;
        [SerializeField]
        EventMode mode;
        [SerializeField]
        GenericBehaviour[] behaviourList;

        private bool behaved = false;

        bool CheckEventAndBehave (EventType type)
        {
            if (type != this.type)
                return false;

            if (mode == EventMode.Once && behaved)
                return false;

            Behave ();
            return true;
        }

        void OnMouseUpAsButton()
        {
            CheckEventAndBehave (EventType.OnMouseUpAsButton);
        }

        void OnTriggerEnter (Collider other)
        {
            CheckEventAndBehave (EventType.OnTriggerEnter);
        }

        void OnTriggerExit (Collider other)
        {
            CheckEventAndBehave (EventType.OnTriggerExit);
        }

        void Behave ()
        {

            behaved = true;

            if (behaviourList == null)
                return;
                
            foreach (GenericBehaviour behaviour in behaviourList)
            {
                if (behaviour != null)
                    behaviour.Behave ();
            }
        }
    }
}
