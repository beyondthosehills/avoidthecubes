﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleBehaviourTools
{
    public class ShowMessageBehaviour : GenericBehaviour 
    {
        [SerializeField] [TextArea]
        string message;
        
        public override void Behave ()
        {
            UIMessageManager.Show (message);
        }
    }
}