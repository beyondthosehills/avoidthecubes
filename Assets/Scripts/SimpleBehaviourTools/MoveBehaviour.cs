﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleBehaviourTools
{
    public class MoveBehaviour : GenericBehaviour 
    {
        [SerializeField]
        Transform targetPosition;
        [SerializeField]
        float duration = 1f;
        
        public override void Behave ()
        {
            if (behaveIsDone)
                return;

            behaveIsDone = true;
            iTween.MoveTo (this.gameObject, targetPosition.position, duration);
        }
    }
}